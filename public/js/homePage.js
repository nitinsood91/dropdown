var last_key_pressed;
var homePage={
	dropdown:function(){
		var textarea=document.querySelector(".textarea");
		var menu=document.querySelector('.dropdown');

		document.addEventListener('click',function(events){
			if(document.querySelector('.dropdown_container').contains(events.target)){
				menu.style.display="block";
			}
			else{
				menu.style.display="none";
			}
		});
	},
	checkbox_onclick:function(){
		var elements=document.querySelectorAll(".dropdown li input");
		var element_label=document.querySelectorAll(".dropdown li label");
		onchange_event= new Event('change');

		for(var i=0;i<elements.length;i++){
			elements[i].addEventListener('change',function(events){
				if(events.target.checked== true){
					this.parentNode.style.background="#dedede";
					var node = document.createElement("li");
					node.setAttribute("class", this.value);
					node.innerHTML="\
						<div>"+this.value+"</div>\
						<span>x</span>\
					";

					document.querySelector(".selected_values").appendChild(node);
					
					document.querySelector(".selected_values ."+events.target.value+" span").onclick=function(event){
						
						events.target.checked = false;
						events.target.dispatchEvent(onchange_event);
						event.stopPropagation();
					};

				}
				else{
					this.parentNode.style.background="none";
					document.querySelector(".selected_values ."+events.target.value).remove();
				}
			});
		}	
	},
	dropdown_textarea_focus:function(){
		var i=0;var j;var first_run=true;
		var textarea=document.querySelector(".textarea");
		
			textarea.onclick=function(){
				var elements=document.querySelectorAll(".dropdown li label");
				homePage.highlight(elements[i]);
			}
		
			textarea.onkeydown=function(){
				var key_pressed=event.key;
				var elements=document.querySelectorAll(".dropdown li label");	

				
				if(key_pressed=='ArrowDown'){
					if (i < elements.length - 1) {
						i++;
						
					}
					else if(i==(elements.length-1)){
						i=0;
					}
					homePage.highlight(elements[i]);
				}
				else if(key_pressed=='ArrowUp'){
					if (i > 0) {
						i--;
					}
					else if(i==0){
						i=elements.length-1
					}
					
					homePage.highlight(elements[i]);
				}
				else if(key_pressed=='Enter'){
					if(last_key_pressed=='ArrowUp'){
						z=i;
					}
					else if(last_key_pressed=='ArrowDown'){
						z=i-1;
					}
					elements[z].childNodes[1].checked=true;
 					elements[z].childNodes[1].dispatchEvent(onchange_event);

				}
				 last_key_pressed=key_pressed;
				
			}
		
	},
	highlight:function(selected_element){
		var elements=document.querySelectorAll(".dropdown li label");	
		console.log(selected_element);
		for(var k=0;k<elements.length;k++){
			elements[k].style.background="white";
		}
		selected_element.style.background="#dedede";
	}
};


document.body.onload=function(){
	homePage.dropdown();
	homePage.checkbox_onclick();
	homePage.dropdown_textarea_focus();
}  

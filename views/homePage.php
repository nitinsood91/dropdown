<html>
	<head>
		<link rel="stylesheet" href="/dropdown/public/css/homePage.css">
	</head>
	<body>
		<div class="wrapper">
			<label class="heading">Dropdown with Multiple selections</label>
			<div class="dropdown_container">
				<div class="textarea" contenteditable="true">
					<ul class="selected_values">	
					</ul>
				</div>
				<ul class="dropdown">
						<li class="a10">
							<label>
								<input type="checkbox" class="dropdown_checkbox a10" value="a10">
								a10
							</label>
						</li>
						<li class="b11">
							<label>
								<input type="checkbox" class="dropdown_checkbox" value="b11">
								b11
							</label>
						</li>
						<li class="c12">
							<label>
								<input type="checkbox" class="dropdown_checkbox" value="c12">
								c12
							</label>
						</li>
						<li class="d13">
							<label>
								<input type="checkbox" class="dropdown_checkbox" value="d13">
								d13
							</label>
						</li>
						<li class="e14">
							<label>
								<input type="checkbox" class="dropdown_checkbox" value="e14">
								e14
							</label>
						</li>
						<li class="f15">
							<label>
								<input type="checkbox" class="dropdown_checkbox" value="f15">
								f15
							</label>
						</li>
						<li class="g16">
							<label>
								<input type="checkbox" class="dropdown_checkbox" value="g16">
								g16
							</label>
						</li>
				</ul>
			</div>	
		</div>
		<script type="text/javascript" src="/dropdown/public/js/homePage.js"></script>
	</body>
</html>